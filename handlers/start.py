from aiogram import Router, F
from aiogram.filters import CommandStart
from aiogram.types import CallbackQuery, Message

from keyboards.inline import get_inline_keyboard
from middlewares.throttling import ThrottlingMiddleware
from loader import redis

start_router = Router()
start_router.message.middleware(ThrottlingMiddleware(redis))

@start_router.message(CommandStart())
async def start(message: Message) -> None:
    """Sample start function"""
    markup = await get_inline_keyboard()
    await message.answer(text="Hello World!", reply_markup=markup)


@start_router.callback_query(F.data=="button")
async def show_alert(callback_query: CallbackQuery) -> None:
    """Show alert message."""
    await callback_query.answer(text="Alert", show_alert=True)
