from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder

async def get_inline_keyboard() -> InlineKeyboardMarkup:
    """Creates sample keyboard that includes the callback_data parameter."""
    builder = InlineKeyboardBuilder()
    builder.add(InlineKeyboardButton(text="Text", callback_data="button"))
    return builder.as_markup()
