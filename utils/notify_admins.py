import logging

from aiogram import Bot

from data.config import ADMINS

async def on_startup_notify(bot: Bot) -> None:
    """Notify admins on bot startup."""
    for admin in ADMINS:
        try:
            await bot.send_message(admin, "Бот запущен.")
        except Exception as e:
            logging.exception(e)
