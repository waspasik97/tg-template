from environs import Env

env = Env()
env.read_env()

BOT_TOKEN = env.str("BOT_TOKEN")
BOT_TOKEN_DEV = env.str("BOT_TOKEN_DEV", BOT_TOKEN)
ADMINS = env.list("ADMINS")

REDIS_HOST=env.str("REDIS_HOST")
REDIS_PORT=env.str("REDIS_PORT")
REDIS_DB=env.str("REDIS_DB")
